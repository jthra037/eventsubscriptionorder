﻿using System;

namespace EventSubscriptionOrder
{
    class Program
    {
        static void Main(string[] args)
        {
            OriginalFlow();
            NewFlow();
            FireFlow();
            FubarFirstFlow();
        }

        private static void OriginalFlow()
        {
            Console.WriteLine("This is what I did originally:\n");
            Foo foo = new Foo();
            Bar bar = new Bar();
            bar.SubscribeB(foo);
            Fubar fubar = new Fubar();
            fubar.SubscribeC(bar);

            foo.Fire();
            Console.Write("\n");
        }
        private static void NewFlow()
        {
            Console.WriteLine("Create everything but subscription order remains the same:\n");
            Foo foo = new Foo();
            Bar bar = new Bar();
            Fubar fubar = new Fubar();
            bar.SubscribeB(foo);
            fubar.SubscribeC(bar);

            foo.Fire();
            Console.Write("\n");
        }

        private static void FubarFirstFlow()
        {
            Console.WriteLine("Create everything and flip subscription order:\n");
            Foo foo = new Foo();
            Bar bar = new Bar();
            Fubar fubar = new Fubar();
            fubar.SubscribeC(bar);
            bar.SubscribeB(foo);

            foo.Fire();
            Console.Write("\n");
        }

        private static void FireFlow()
        {
            Console.WriteLine("Same new and subscribe order but subscribes Bar.Fire instead of Bar.B:\n");
            Foo foo = new Foo();
            Bar bar = new Bar();
            bar.SubscribeFire(foo);
            Fubar fubar = new Fubar();
            fubar.SubscribeC(bar);

            foo.Fire();
            Console.Write("\n");
        }
    }
}
