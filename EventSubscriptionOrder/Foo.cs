﻿using System;

namespace EventSubscriptionOrder
{
    class Foo
    {
        public event Action A = () => { Console.WriteLine("Foo.A called"); };

        public void Fire()
        {
            A();
        }
    }
}
