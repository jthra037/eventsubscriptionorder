﻿using System;

namespace EventSubscriptionOrder
{
    class Fubar
    {
        public void SubscribeC(Bar bar)
        {
            bar.B += C;
        }

        private void C()
        {
            Console.WriteLine("Fubar.C called");
        }
    }
}
