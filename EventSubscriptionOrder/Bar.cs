﻿using System;

namespace EventSubscriptionOrder
{
    class Bar
    {
        public event Action B = () => { Console.WriteLine("Bar.B called"); };

        public void SubscribeB(Foo foo)
        {
            foo.A += B;
        }

        public void SubscribeFire(Foo foo)
        {
            foo.A += Fire;
        }

        public void Fire()
        {
            B();
        }
    }
}
